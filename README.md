# Simple Swap Contracts

Contracts intended as Bitcoin Cash CashTokens DEX building blocks.
They verify the minimum to ensure trustless swaps, and it is the responsibility of apps which use them to ensure that funds don't get lost or burned when creating, filling or cancelling trades.

## Simple Swap for Bitcoin Cash (BCH) CashTokens

### v2.0.0

The contract address will encode the maker's blind offer.
The maker sets his cancel/payout key and a list of wants, then generates the contract P2SH address and funds it with his offering.
Taker may spend from the contract to take the offering only if he creates an output that satisfies the wants and funds the TX with inputs that will balance the TX.
The output which satisfies the wants MUST be placed on the same output index as this contract's input's index.

The contract may be used to swap any 2 UTXOs but it is most suitable for swapping NFTs since the offer may either be taken in whole or not at all, it does not support partial fills when used to swap fungible tokens (FTs).

It is strongly advised that apps record the contract parameters in an `OP_RETURN` when they create the offer so redeem script may be reconstructed.
Losing the parameters would likely result in loss of funds.
Suggestion:

- `OP_RETURN <'CHSC'> <0> <sha256(redeem_script_tail)> <wantSats> <wantCategoryID> <wantNFTCommitment> <wantFTs> <ownerPubKey>` ,

where `redeem_script_tail` is the bytecode of `.asm` file resulting from `cashc` compilation.
The sha256hash can be obtained in shell by: `cashc -h "contract.cash" | xxd -r -p | sha256sum` .
The redeem script can then be simply reconstructed by splitting the `OP_RETURN` output at byte 8 and appending the known tail of the contract.

It is app's responsibility to produce a correct output that "captures" the offering for the taker.
Failure to include the taker's outputs will burn the offering he just purchased.

Similarly, it is app's responsibility to produce a correct output that takes back the offering when cancelling offers.

## License

[MIT No Attribution](https://opensource.org/license/mit-0/).
